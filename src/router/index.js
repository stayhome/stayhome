import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '@/containers/Full'
import Front from '@/containers/Front'

// Views
import Dashboard from '@/views/Dashboard'
import Home from '@/views/Home'
import Chat from '@/views/Chat'

Vue.use(Router)

export default new Router({
  mode: 'history',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/dashboard',
      name: 'Home',
      component: Full,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        }

      ]
    },
    {
      path: '/',
      name: 'Home',
      component: Front,
      children: [
        {
          path: '',
          name: 'Home',
          component: Home
        }

      ]
    },
    {
      path: '/chat',
      name: 'Chat',
      component: Chat,
      children: [
        {
          path: 'chat',
          name: 'chat',
          component: Chat
        }

      ]
    }
  ]
})
